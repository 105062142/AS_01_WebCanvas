var mychoose = "pencil";
var a = "serif";
function myselect(str)
{
    mychoose = str;
    var _canvas = document.getElementById('canvas');
    var ctx = _canvas.getContext('2d');  
    if(mychoose == "pencil") _canvas.style.cursor = "url('pencil(cursor).png'),auto";
    else if(mychoose == "eraser") _canvas.style.cursor = "url('eraser(cursor).png'),auto";
    else if(mychoose == "wannatext")
    {
        _canvas.style.cursor = "text";
        wannatext();
    }
    else if(mychoose == "circle") _canvas.style.cursor = "url('circle(cursor).png'),auto";
    else if(mychoose == "triangle") _canvas.style.cursor = "url('triangle(cursor).png'),auto";
    else if(mychoose == "square") _canvas.style.cursor = "url('rectangle(cursor).png'),auto";
    else _canvas.style.cursor = "auto";
}

function draw()
{   
    var _canvas = document.getElementById('canvas');
    var ctx = _canvas.getContext('2d');  
    var beginx,beginy,endx,endy;

    //choose color
    document.getElementById("color").addEventListener("change",changecolor);
    
    function changecolor()
    {
        ctx.strokeStyle = document.getElementById("color").value;
    }

    //reset
    document.getElementById('clear').addEventListener
    ('click', 
    function() 
    {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        cStep = -1;
    }
    , false);

    //brush-size
    document.getElementById("brush-size").addEventListener("change",changesize);
    
    function changesize()
    {
        ctx.lineWidth = document.getElementById("brush-size").value;
    }

    var x = 0;
    var y = 0;
    function getMousePos(canvas, evt) 
    {
        var rect = canvas.getBoundingClientRect();
        return {
            x: evt.clientX - rect.left,
            y: evt.clientY - rect.top
        };   
    };

    function mouseMove(evt) 
    {
        switch(mychoose)
        {
            case "pencil":
                ctx.globalCompositeOperation = "source-over";
                _canvas.onclick = function(e) {return;}
                var mousePos = getMousePos(_canvas, evt);
                ctx.lineTo(mousePos.x, mousePos.y + 20);
                ctx.stroke();
            break;
            case "eraser":
                ctx.globalCompositeOperation = "destination-out";
                _canvas.onclick = function(e) {return;}
                var mousePos = getMousePos(_canvas, evt);
                ctx.lineTo(mousePos.x, mousePos.y + 20);
                ctx.stroke();
            break;
            case "square":
                ctx.globalCompositeOperation = "source-over";
                _canvas.onclick = function(e) {return;}
            break;
            case "circle":
                ctx.globalCompositeOperation = "source-over";
                _canvas.onclick = function(e) {return;}
            break;
            case "triangle":
                ctx.globalCompositeOperation = "source-over";
                _canvas.onclick = function(e) {return;}
            break;
            default:  
                ctx.globalCompositeOperation = "source-over";
                //_canvas.onclick = function(e) {return;}
                var mousePos = getMousePos(_canvas, evt);
                /*ctx.lineTo(mousePos.x, mousePos.y);
                ctx.stroke(); */ 
            break;
        }
    };

    canvas.addEventListener('mousedown', 
        function(evt) 
        {
            var mousePos = getMousePos(_canvas, evt);
            evt.preventDefault();
            ctx.beginPath();
            ctx.moveTo(mousePos.x, mousePos.y + 20);  
            beginx = mousePos.x;
            beginy = mousePos.y + 20;
            canvas.addEventListener('mousemove', mouseMove, false);
        }
    );


    canvas.addEventListener('mouseup', 
    function(evt) 
    {
        canvas.removeEventListener('mousemove', mouseMove, false);
        var mousePos = getMousePos(_canvas, evt);
                endx = mousePos.x;
                endy = mousePos.y;
            if(mychoose == "square")
            {
                if(beginx < endx && beginy < endy)
                {
                    ctx.rect(beginx,beginy,endx - beginx,endy - beginy);
                    ctx.stroke();
                }
                
                else if(beginx < endx && beginy > endy)
                {
                    ctx.rect(beginx,endy,endx - beginx,beginy - endy);
                    ctx.stroke();
                }
                
                else if(beginx > endx && beginy > endy)
                {
                    ctx.rect(endx,endy,beginx - endx,beginy - endy);
                    ctx.stroke();
                }
                else
                {
                    ctx.rect(endx,beginy,beginx - endx,endy - beginy);
                    ctx.stroke();
                }
            }   
            else if(mychoose == "circle")
            {
                ctx.beginPath();
                ctx.arc(beginx,beginy,Math.sqrt((endx - beginx) * (endx - beginx) + (endy - beginy) * (endy - beginy)), 0, 2 * Math.PI);
                ctx.stroke();
            }
            else if(mychoose == "triangle")
            {
                if(beginx < endx && beginy < endy)
                {
                    ctx.beginPath();
                    ctx.moveTo((beginx + endx) / 2, beginy);
                    ctx.lineTo(endx, endy);
                    ctx.lineTo(beginx, endy);
                    ctx.closePath();
                    ctx.stroke();
                }
                else if(beginx > endx && beginy > endy)
                {
                    ctx.beginPath();
                    ctx.moveTo((beginx + endx) / 2, endy);
                    ctx.lineTo(beginx, beginy);
                    ctx.lineTo(endx, beginy);
                    ctx.closePath();
                    ctx.stroke();
                }
                else if(beginx < endx && beginy > endy)
                {
                    ctx.beginPath();
                    ctx.moveTo((beginx + endx) / 2, endy);
                    ctx.lineTo(endx, beginy);
                    ctx.lineTo(beginx, beginy);
                    ctx.closePath();
                    ctx.stroke();
                }
                else
                {
                    ctx.beginPath();
                    ctx.moveTo((beginx + endx) / 2,beginy);
                    ctx.lineTo(beginx, endy);
                    ctx.lineTo(endx, endy);
                    ctx.closePath();
                    ctx.stroke();
                }
            }
        cPush();
                
    }, false);
}

function myFunction(x) {
    var whichSelected = x.selectedIndex;
    a = x.options[whichSelected].text;
}


//text input
function wannatext()
{
   var canvas = document.getElementById('canvas'),
   ctx = canvas.getContext('2d');
  // font = '14px sans-serif',
   var hasInput = false;

   //start here
   ctx.globalCompositeOperation = "source-over";
    var font = document.getElementById("font-size").value + 'px ' + a;//document.getElementById("font-style").value;
    ctx.fillStyle = document.getElementById("color").value;

   
    document.getElementById("font-size").addEventListener("change",fontchange);
    document.getElementById("font-style").addEventListener("change",fontchange);
    document.getElementById("color").addEventListener("change",fontchange);

    function fontchange()
    {
        font = document.getElementById("font-size").value + 'px ' + a;//document.getElementById("font-style").value;
        ctx.fillStyle = document.getElementById("color").value;
    }
    //stop here

    canvas.onclick = function(e) {
        if (hasInput) return;
        addInput(e.clientX, e.clientY);
    }

    function addInput(x, y) {
   
   var input = document.createElement('input');
   
   input.type = 'text';
   input.style.position = 'fixed';
   input.style.left = x  + 'px';
   input.style.top = y  + 'px';

   input.onkeydown = handleEnter;
   
   document.body.appendChild(input);

   input.focus();
   
   hasInput = true;
}

    function handleEnter(e) {
   var keyCode = e.keyCode;
   if (keyCode === 13) {
       drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
       document.body.removeChild(this);
       hasInput = false;
   }
}

    function drawText(txt, x, y) {
    var rect = canvas.getBoundingClientRect();
    ctx.textBaseline = 'top';
    ctx.textAlign = 'left';
    ctx.font = font;
    ctx.fillText(txt, x - rect.left, y - rect.top);
    cPush();
}
}

//download
function downloadthepic()
{
    var image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");  // here is the most important part because if you dont replace you will get a DOM 18 exception.
    var link = document.createElement('a');
    link.href = image;
    link.download = "canvas.png";
    var event = document.createEvent('MouseEvents');
    event.initMouseEvent('click',true,false,window,0,0,0,0,0,false,false,false,false,0,null);
    link.dispatchEvent(event);
}

//upload
function uploadthepic()
{
    var imageLoader = document.getElementById('imageLoader');
    imageLoader.addEventListener('change', handleImage, false);
    var canvas = document.getElementById('canvas');
    var ctx = canvas.getContext('2d');


function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
           // canvas.width = img.width;
            //canvas.height = img.height;
            ctx.drawImage(img,0,0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);   
    cPush();  
}
}

//redo undo

var cPushArray = new Array();
var cStep = -1;
var ctx;
	
function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById('canvas').toDataURL());
}

function cUndo() {

    var _canvas = document.getElementById('canvas');
    var ctx = _canvas.getContext('2d');
    ctx.globalCompositeOperation = "source-over";
    console.log(cStep);
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
    
}

function cRedo() {
    var _canvas = document.getElementById('canvas');
    var ctx = _canvas.getContext('2d');
    ctx.globalCompositeOperation = "source-over";
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}

