# Software Studio 2018 Spring Assignment 01 Web Canvas

## Basic components
1. **Brush and eraser**
    筆刷以及橡皮擦的部分，做法相同，都是偵測滑鼠按下、移動以及放開，並在移動過程中不斷取點，
    從按下點開始無限個兩點連線，直到放開點，畫出一條不規則線，橡皮擦的差異只在於，筆刷顏色是背景顏色，也就是可以蓋掉圖案。
2. **color selector**
    顏色選擇部分，使用input中的color，將值傳到筆刷顏色，便能實現選取顏色的功能。
3. **brush size**
    筆刷粗細部分，使用input中的range，將值傳到筆刷大小，實現筆刷粗細功能。
4. **text(typeface and size)**
    輸入字的部分，在滑鼠按下地方開出一個能輸入文字的方框，輸入完文字按下enter，便會將輸入的文字印在canvas上，
    若未輸入文字但想關掉方框，直接按下enter即可。選取字型和大小的部分，大小一樣是使用input中的range，字型則使用select讓使用者選取。
5. **cursor icon**
    滑鼠游標會在按下時傳送特定值到function內，使用url換成特定png檔。
6. **refresh**
    按下按鈕後，原先畫布會被一塊等大全白的畫布覆蓋，藉以實現refresh功能。

## Advance tools
1. **circle,rectangle,triangle**
    circle:
        滑鼠按下部分是畫出圓的圓心，從按下到放開的距離是圓的半徑，放開前可任意更改圓的半徑，放開後顯現圓形。
        利用函式arc畫出圓。
    rectangle:
        滑鼠按下到放開拉出長方形的對角線，放開後顯現長方形。
        利用函式rect畫出長方形。
    triangle:
        分四個方向討論:
            1.左上到右下:按下點是三角形頂點以及左下方點的交點，放開點是右下方點。
            2.右下到左上:按下點是右下方點，放開點是三角形頂點以及左下方的交點。
            3.左下到右上:按下點是左下方點，放開點是三角形頂點以及右下方的交點。
            4.右上到左下:按下點是三角形頂點以及右下方點的交點，放開點是左下方點。
        都是放開後顯現等腰三角形。
        利用beginpath、moveto、lineto、closepath畫出三角形。
2. **un-do and re-do button**
    使用圖層概念，ud-do就是回復上一個動作，拿掉一層圖層，也就是在每一個動作做完就要存一個圖層，re-do便是放上一層圖層。
3. **image tool**
    利用input中的file，將上傳的圖片印在canvas的左上角。
4. **download**
    利用toDataURL後再轉換，將canvas存成png檔。





